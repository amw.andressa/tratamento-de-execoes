package entities.application;

import entities.Account;
import exceptions.BusinessException;

import java.util.Locale;
import java.util.Scanner;

public class Program {

    public static void main(String[] args) {

        Locale.setDefault(Locale.US);
        Scanner sc = new Scanner(System.in);

        System.out.println("Informe os dados da conta: ");
        System.out.print("Numero: ");
        int numero = sc.nextInt();
        System.out.print("Titular: ");
        sc.nextLine();
        String titular = sc.nextLine();
        System.out.print("Saldo inicial: ");
        double saldoInicial = sc.nextDouble();
        System.out.print("Limite de saque: ");
        double limiteDeSaque = sc.nextDouble();

        Account account = new Account(numero, titular, saldoInicial, limiteDeSaque);

        System.out.println();

        System.out.print("Informe uma quantia para sacar: ");
        double saque = sc.nextDouble();

        try{
            account.withdraw(saque);
            System.out.printf("Novo saldo: %.2f", account.getBalance());
        }
        catch (BusinessException e){
            System.out.println(e.getMessage());
        }
        sc.close();
    }
}
